output "argocd_ssh_public_key" {
  value = tls_private_key.argocd_default_ssh_key.public_key_openssh
}
