data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

/*
  Data from created k8s cluster
*/
data "aws_eks_cluster" "this" {
  name = var.k8s_cluster_name
}

data "aws_eks_cluster_auth" "this" {
  name = var.k8s_cluster_name
}
