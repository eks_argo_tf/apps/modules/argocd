variable "argocd_hostname" {
  description = "ArgoCD hostname for ingress"
  type        = string
  default     = ""
}

variable "argocd" {
  description = "ArgoCD configuration"
  default     = {}

  type = object({
    # Chart version
    version             = optional(string, "5.9.1")
    # ArgoCD Apps chart version
    argocd_apps_version = optional(string, "0.0.1")
    # ArgoCD Image tag
    image_tag           = optional(string, "v2.5.7")
    # We need bcrypted password and because it is changing each time we can't generate it in code
    # You can create this hash with `htpasswd -nbBC 10 "" $ARGO_PWD | tr -d ':\n' | sed 's/$2y/$2a/'`
    admin_password_hash = optional(string, "")
    # Namespace
    namespace           = optional(string, "argocd")

    # Default applications like main app-of-apps or infrustructure application
    additional_applications = optional(list(object({
      # Application name
      name       = string
      # Application repo
      repoUrl    = string
      # Path to the chart in repo
      path       = string
      # Value file
      valueFiles = optional(string, "")
    })), [])

    # Enable RO aceess without authentification
    enable_ro_access = optional(bool, false)
    # ArgoCD server aditional configuration. User accounts can be specified here
    server_config    = optional(string, "")
    # Configure user policy https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/rbac.md
    user_policy      = optional(string, "")
    # ArgoCD server parameters for the values files
    server_params    = optional(string, "")
  })
}

variable "ldap" {
  description = "LDAP configuration"
  default     = {}

  type = object({
    enabled                  = optional(bool, false)
    name                     = optional(string, "")  # LDAP server
    host                     = optional(string, "")  # DN which will be used to find login user ldap record
    users_search_dn          = optional(string, "")  # LDAP users filters
    users_search_filter      = optional(string, "")  # DN which will be used to find groups ldap records
    groups_search_dn         = optional(string, "")
    groups_search_filter     = optional(string, "")  # LDAP groups filters
    service_account_dn       = optional(string, "")  # Login to LDAP server with this DN
    service_account_password = optional(string, "")  # Login to LDAP server with this password
  })
}

variable "okta" {
  description = "saml configuration"
  default     = {}

  type = object({
    enabled = optional(bool, false)
    ssoURL  = optional(string, "")
    caData  = optional(string, "")
  })
}

variable "prometheus_selector_label" {
  description = "Prometheus selector label"
  type        = string
  default     = "default"
}

variable "default_chartmuseum" {
  description = "Default chartmuseum"
  default = {
    url = ""
  }
}

variable "k8s_cluster_name" {
  description = "Name of EKS cluster provided by COPS. Will trigger toolset installation if not empty"
}

variable "location" {
  description = "location"
  type        = string
}

variable "certificate_arn" {
  description = "Internal certificate ARN"
  type        = string
  default     = ""
}

variable "component" {
  description = "Component name"
  type        = string
  default     = "kbr"
}
