resource "helm_release" "argocd" {
  name             = "argocd"
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  version          = var.argocd.version
  namespace        = kubernetes_namespace.argocd.id
  atomic           = true
  wait             = true
  create_namespace = false

  values = [
    templatefile("${path.module}/argocd_template.yaml", {
      argocd                    = var.argocd
      argocd_hostname           = var.argocd_hostname
      ldap                      = var.ldap
      okta                      = var.okta
      namespace                 = kubernetes_namespace.argocd.id
      location                  = var.location
      default_chartmuseum_url   = lookup(var.default_chartmuseum, "url")
      prometheus_selector_label = var.prometheus_selector_label
      certificate_arn           = var.certificate_arn
      component                 = var.component
    })
  ]
}

resource "helm_release" "argocd_apps" {
  name       = "argocd-apps"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argocd-apps"
  version    = var.argocd.argocd_apps_version
  namespace  = kubernetes_namespace.argocd.id
  atomic     = true

  depends_on = [
    helm_release.argocd
  ]

  values = [
    templatefile("${path.module}/applications_template.yaml", {
      argocd   = var.argocd
      location = var.location
    })
  ]
}

resource "kubernetes_namespace" "argocd" {
  metadata {
    name = "argocd"
  }
}

resource "tls_private_key" "argocd_default_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "kubernetes_secret" "argocd_default_ssh_key" {
  metadata {
    name      = "argocd-default-ssh-key"
    namespace = kubernetes_namespace.argocd.id
  }

  data = {
    sshPrivateKey = tls_private_key.argocd_default_ssh_key.private_key_pem
  }
}
